# Análisis de Datos y Visualización con Python

- Docente, Mg. Ing. Emiliano López.

Curso de Python para procesamiento, análisis y visualización de datos

En este curso se brindarán los conceptos fundamentales de la programación estructurada orientada al procesamiento y análisis de datos haciendo hincapié en el pensamiento computacional. Inicialmente se trabajará en el procesamiento de datos utilizando las bilbioteca estándar para luego adentrarse en las aquellas pertenecientes al ecosistema científico de Python. 

Se proveerán las herramientas necesarias para generar un entorno computacional para ciencia de datos asegurando su replicabilidad en diferentes entornos y a lo largo del tiempo.  Se trabará con datasets públicos reales, para explorar, corregir, procesar, filtrar, extraer y generar nueva información numérica, categórica y gráfica.

Se hará hincapié en la visualización de datos para representarlos tanto en gráficos 1D, 2D y 3D como sobre mapas.  

## Duración

Serán 10 clases de 2.30hs cada una. 

## Modalidad

Las clases serán online sincrónicas utilizando la plataforma Jitsi Meet. Se grabarán y estarán disponible para su posterior reproducción. En caso que sea factible podrían ser presenciales.

Cada tema estará contenido en un notebook jupyter ``.ipynb`` sobre el que se trabajará en cada clase junto a los archivos de datos correspondientes. 

A su vez lxs alumnxs trabajarán sobre un notebook jupyter donde el docente los irá guiando en cada clase. 

Luego de las clases en vivo, la forma de comunicación será a través de un foro, un grupo de Telegram y un repositorio en Gitlab.

## Aprobación

El curso se aprobará realizando un trabajo final integrador donde se apliquen los conceptos dictados en el curso. 

## Tabla de Contenidos 

- Instalación del intérprete, exploración de IDEs y notebooks
- Archivos de texto: lectura y escritura
- Parseo de datos: split()
- Tipos de datos: str, int, float
- Contenedores: listas y diccionarios
- Iteraciones: for, while
- Condicionales: if, if-else, if-elif-else
- Funciones de la biblioteca estándar
- Numpy, vectores eficientes, 
- Operaciones con nd-array
- Scipy, estadística descriptiva
- Visualización con matplotlib 
- Graficación de series temporales, estadísticas
- Análisis de datos con Pandas
- Dataframes a partir de datos, diccionarios
- Filtrado, boolean indexing
- Visualización interactiva
- Mapeo de datos con plotly y folium

## Distribución de temas por clases

Las primeras 3 o 4 clases serán destinadas a crear el entorno de programación, evaluar las alternativas disponibles y se tratarán los conceptos básicos de la programación estructurada focalizando en el pensamiento computacional. 

Las clases restantes serán destinadas al procesamiento y filtrado de datos utilizando bibliotecas específicas que hacen al ecosistema científico de Python.

## Bibliografía

No nos basaremos en un recurso específico, pero en general nos será útil consultar:

- https://gitlab.com/emilopez/dev01/tree/master/tuSL/pdfs
- Tutorial oficial de python: http://docs.python.org.ar/tutorial/pdfs/TutorialPython3.pdf
- https://scipy-lectures.org/
- Jake VanderPlas - Python Data Science Handbook Essential Tools for Working with Data-O'Reilly Media (2016)
- Think Stats, Exploratory Data Analysis in Python
- Python for Data Analysis, 2nd Edition


## Ediciones previas
- 2019, Facultad de Bioquímica y Ciencias Biológicas. Universidad Nacional del Litoral.