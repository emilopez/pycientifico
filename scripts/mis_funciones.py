#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 21:07:53 2019

@author: emiliano
"""

def suma_uno(num):
    return num+1

def genera_pass(dificultad=2, longitud=8):
    import random
    letras = []
    for letra in range(ord("a"), ord("z")+1):
        letra = chr(letra)
        if dificultad == 2:
            if random.randint(0, 1) == 0:
                letras.append(letra.upper())
            else:
                letras.append(letra)
        else:
            letras.append(letra)
    
    mipass = random.choices(letras, k=longitud)
    mipass = "".join(mipass)
    return mipass



if __name__ == '__main__':
    print(genera_pass(dificultad = 1, longitud=10))
    print(genera_pass(dificultad = 2, longitud=20))