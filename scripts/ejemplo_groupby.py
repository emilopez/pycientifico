import pandas as pd

"""
Obtiene el promedio por región, luego copia el promedio de pobreza hogares, 
y crea una nueva columna en el dataframe original (PromPobrezaHogares_x_Region)
repitiendo los promedios para fila que pertenezca a una determinada Región
"""

# lee dataset
eph = pd.read_csv("datos/eph_pobreza2019.csv", sep=";")

# agrupa por regiones
regiones = eph.groupby("Regiones")

# calcula el promedio a cada región
promedios_x_regiones = regiones.mean()

# crea columna del dataframe para almacenar los promedios
eph["PromPobrezaHogares_x_Region"] = 0

# repite el promedio de una region en cada fila que corresponde a la misma región
for region in eph["Regiones"].unique():
    # obtiene el índice de cada región
    ind = eph["Regiones"] == region
    # obtiene el índice de esa región en el agrupamiento obtenido, ojo, la columna por la que agrupamos es un índice ahora
    ind_grupo = promedios_x_regiones.index == region
    # extrae el valor del promedio de esa serie
    promedio = promedios_x_regiones[ind_grupo]["PobrezaHogares"][0]
    # asigna a todos las filas del dataframe de esa Region
    eph["PromPobrezaHogares_x_Region"][ind] = promedio