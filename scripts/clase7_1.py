#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  2 13:50:59 2019

@author: emiliano
"""

import pandas as pd
df = pd.read_csv("/home/emiliano/data/docencia/2019-PythonCienciaFBCB/pycientifico/datos/eph_pobreza2019.csv", 
                 sep=";", skiprows=1,
                 names=["regiones", 
                          "subregiones",
                          "ph", 
                          "pp", 
                          "ih", 
                          "ip"])
suma_x_region = df.groupby("regiones").sum()
maximo_x_region = df.groupby("regiones").max()
    